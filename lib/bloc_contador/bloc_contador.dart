import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:nuevo_proyecto_2/app/constantes.dart';
import 'package:nuevo_proyecto_2/bloc_contador/estados.dart';
import 'package:nuevo_proyecto_2/bloc_contador/eventos.dart';
import 'package:nuevo_proyecto_2/dominio/interfase_repositorios.dart';

class BlocContador extends Bloc<Evento, Estado2> {
  
  int numero = 0;
  int numeralSeleccionado = 0;
  int deporteSeleccionado = 0;
  InterfaseRepositorios repositorio;

  BlocContador(this.repositorio) : super(Iniciandome()) {
    on<Inicializado>(_onInicializado);

    on<Modificado>(_onModificado);
    on<ModificadoNumerales>(_onModificadodeNumerales);
    on<RepositorioCambiado>(_onRepositorioCambiado);
    on<ModificadoDeportes>(_onModificadoDeportes);

    add(Inicializado());
  }


  void _onModificado(Modificado evento, Emitter<Estado2> emit) {
    numero = numero + evento.enCuanto;
    emit(dependeNumero(numero));
  }

  void _onInicializado(Inicializado evento, Emitter<Estado2> emit) async {
    final valorRecibido = await repositorio.obtenerNumero(minimo: 3, maximo: 8);

    valorRecibido.fold((cadena){
      emit(Problema(cadena));
    }, (entero){
      numero = entero;
      emit(dependeNumero(numero));
    });
  }

  Estado2 dependeNumero(int numero) {
    if ((numero == numeroPele) && (deporteSeleccionado == 0)) return Imagen(direccionImagenpele);
    if ((numero == numeroLarry) && (deporteSeleccionado == 1)) return Imagen(direccionImagenlarry);
    return Numero(numero, numeralSeleccionado);
     
  }

  FutureOr<void> _onModificadodeNumerales(ModificadoNumerales event, Emitter<Estado2> emit) {
    numeralSeleccionado = event.cualNumeral;
    emit(dependeNumero(numero));

  }

  FutureOr<void> _onRepositorioCambiado(RepositorioCambiado event, Emitter<Estado2> emit) {

    repositorio = event.repositorio;
    add(Inicializado());

  }

  FutureOr<void> _onModificadoDeportes(ModificadoDeportes event, Emitter<Estado2> emit) {
    deporteSeleccionado = event.cualDeporte;
    emit(dependeNumero(numero));

  }
}

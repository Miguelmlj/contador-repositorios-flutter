import 'package:nuevo_proyecto_2/dominio/interfase_repositorios.dart';

class Evento{}

class Inicializado extends Evento{}

class Modificado extends Evento{
  final int enCuanto;

  Modificado(this.enCuanto);
}

class ModificadoNumerales extends Evento{
  final int cualNumeral;

  ModificadoNumerales(this.cualNumeral);
}

class RepositorioCambiado extends Evento{
  final InterfaseRepositorios repositorio;

  RepositorioCambiado(this.repositorio);
}

class ModificadoDeportes extends Evento{
  final int cualDeporte;

  ModificadoDeportes(this.cualDeporte);
}


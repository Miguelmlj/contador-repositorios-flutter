import 'package:freezed_annotation/freezed_annotation.dart';

part 'estados.freezed.dart';

// class Estado{}

// class Iniciandome extends Estado{}

// class Numero extends Estado{
//   final int numero;

//   Numero(this.numero);
// }

// class Imagen extends Estado{
//   final String nombre;

//   Imagen(this.nombre);

// }



@freezed
class Estado2 with _$Estado2{
  const factory Estado2() = Iniciandome;
  const factory Estado2.numero(int numero, int tipoNumeral) = Numero;
  const factory Estado2.imagen(String nombre) = Imagen;
  const factory Estado2.problema(String mensaje) = Problema;
}
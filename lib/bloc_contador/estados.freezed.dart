// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'estados.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$Estado2TearOff {
  const _$Estado2TearOff();

  Iniciandome call() {
    return const Iniciandome();
  }

  Numero numero(int numero, int tipoNumeral) {
    return Numero(
      numero,
      tipoNumeral,
    );
  }

  Imagen imagen(String nombre) {
    return Imagen(
      nombre,
    );
  }

  Problema problema(String mensaje) {
    return Problema(
      mensaje,
    );
  }
}

/// @nodoc
const $Estado2 = _$Estado2TearOff();

/// @nodoc
mixin _$Estado2 {
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(int numero, int tipoNumeral) numero,
    required TResult Function(String nombre) imagen,
    required TResult Function(String mensaje) problema,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciandome value) $default, {
    required TResult Function(Numero value) numero,
    required TResult Function(Imagen value) imagen,
    required TResult Function(Problema value) problema,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Estado2CopyWith<$Res> {
  factory $Estado2CopyWith(Estado2 value, $Res Function(Estado2) then) =
      _$Estado2CopyWithImpl<$Res>;
}

/// @nodoc
class _$Estado2CopyWithImpl<$Res> implements $Estado2CopyWith<$Res> {
  _$Estado2CopyWithImpl(this._value, this._then);

  final Estado2 _value;
  // ignore: unused_field
  final $Res Function(Estado2) _then;
}

/// @nodoc
abstract class $IniciandomeCopyWith<$Res> {
  factory $IniciandomeCopyWith(
          Iniciandome value, $Res Function(Iniciandome) then) =
      _$IniciandomeCopyWithImpl<$Res>;
}

/// @nodoc
class _$IniciandomeCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $IniciandomeCopyWith<$Res> {
  _$IniciandomeCopyWithImpl(
      Iniciandome _value, $Res Function(Iniciandome) _then)
      : super(_value, (v) => _then(v as Iniciandome));

  @override
  Iniciandome get _value => super._value as Iniciandome;
}

/// @nodoc

class _$Iniciandome implements Iniciandome {
  const _$Iniciandome();

  @override
  String toString() {
    return 'Estado2()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Iniciandome);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(int numero, int tipoNumeral) numero,
    required TResult Function(String nombre) imagen,
    required TResult Function(String mensaje) problema,
  }) {
    return $default();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
  }) {
    return $default?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciandome value) $default, {
    required TResult Function(Numero value) numero,
    required TResult Function(Imagen value) imagen,
    required TResult Function(Problema value) problema,
  }) {
    return $default(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
  }) {
    return $default?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(this);
    }
    return orElse();
  }
}

abstract class Iniciandome implements Estado2 {
  const factory Iniciandome() = _$Iniciandome;
}

/// @nodoc
abstract class $NumeroCopyWith<$Res> {
  factory $NumeroCopyWith(Numero value, $Res Function(Numero) then) =
      _$NumeroCopyWithImpl<$Res>;
  $Res call({int numero, int tipoNumeral});
}

/// @nodoc
class _$NumeroCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $NumeroCopyWith<$Res> {
  _$NumeroCopyWithImpl(Numero _value, $Res Function(Numero) _then)
      : super(_value, (v) => _then(v as Numero));

  @override
  Numero get _value => super._value as Numero;

  @override
  $Res call({
    Object? numero = freezed,
    Object? tipoNumeral = freezed,
  }) {
    return _then(Numero(
      numero == freezed
          ? _value.numero
          : numero // ignore: cast_nullable_to_non_nullable
              as int,
      tipoNumeral == freezed
          ? _value.tipoNumeral
          : tipoNumeral // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$Numero implements Numero {
  const _$Numero(this.numero, this.tipoNumeral);

  @override
  final int numero;
  @override
  final int tipoNumeral;

  @override
  String toString() {
    return 'Estado2.numero(numero: $numero, tipoNumeral: $tipoNumeral)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Numero &&
            (identical(other.numero, numero) ||
                const DeepCollectionEquality().equals(other.numero, numero)) &&
            (identical(other.tipoNumeral, tipoNumeral) ||
                const DeepCollectionEquality()
                    .equals(other.tipoNumeral, tipoNumeral)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(numero) ^
      const DeepCollectionEquality().hash(tipoNumeral);

  @JsonKey(ignore: true)
  @override
  $NumeroCopyWith<Numero> get copyWith =>
      _$NumeroCopyWithImpl<Numero>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(int numero, int tipoNumeral) numero,
    required TResult Function(String nombre) imagen,
    required TResult Function(String mensaje) problema,
  }) {
    return numero(this.numero, tipoNumeral);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
  }) {
    return numero?.call(this.numero, tipoNumeral);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
    required TResult orElse(),
  }) {
    if (numero != null) {
      return numero(this.numero, tipoNumeral);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciandome value) $default, {
    required TResult Function(Numero value) numero,
    required TResult Function(Imagen value) imagen,
    required TResult Function(Problema value) problema,
  }) {
    return numero(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
  }) {
    return numero?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
    required TResult orElse(),
  }) {
    if (numero != null) {
      return numero(this);
    }
    return orElse();
  }
}

abstract class Numero implements Estado2 {
  const factory Numero(int numero, int tipoNumeral) = _$Numero;

  int get numero => throw _privateConstructorUsedError;
  int get tipoNumeral => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NumeroCopyWith<Numero> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImagenCopyWith<$Res> {
  factory $ImagenCopyWith(Imagen value, $Res Function(Imagen) then) =
      _$ImagenCopyWithImpl<$Res>;
  $Res call({String nombre});
}

/// @nodoc
class _$ImagenCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $ImagenCopyWith<$Res> {
  _$ImagenCopyWithImpl(Imagen _value, $Res Function(Imagen) _then)
      : super(_value, (v) => _then(v as Imagen));

  @override
  Imagen get _value => super._value as Imagen;

  @override
  $Res call({
    Object? nombre = freezed,
  }) {
    return _then(Imagen(
      nombre == freezed
          ? _value.nombre
          : nombre // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$Imagen implements Imagen {
  const _$Imagen(this.nombre);

  @override
  final String nombre;

  @override
  String toString() {
    return 'Estado2.imagen(nombre: $nombre)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Imagen &&
            (identical(other.nombre, nombre) ||
                const DeepCollectionEquality().equals(other.nombre, nombre)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(nombre);

  @JsonKey(ignore: true)
  @override
  $ImagenCopyWith<Imagen> get copyWith =>
      _$ImagenCopyWithImpl<Imagen>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(int numero, int tipoNumeral) numero,
    required TResult Function(String nombre) imagen,
    required TResult Function(String mensaje) problema,
  }) {
    return imagen(nombre);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
  }) {
    return imagen?.call(nombre);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
    required TResult orElse(),
  }) {
    if (imagen != null) {
      return imagen(nombre);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciandome value) $default, {
    required TResult Function(Numero value) numero,
    required TResult Function(Imagen value) imagen,
    required TResult Function(Problema value) problema,
  }) {
    return imagen(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
  }) {
    return imagen?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
    required TResult orElse(),
  }) {
    if (imagen != null) {
      return imagen(this);
    }
    return orElse();
  }
}

abstract class Imagen implements Estado2 {
  const factory Imagen(String nombre) = _$Imagen;

  String get nombre => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ImagenCopyWith<Imagen> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProblemaCopyWith<$Res> {
  factory $ProblemaCopyWith(Problema value, $Res Function(Problema) then) =
      _$ProblemaCopyWithImpl<$Res>;
  $Res call({String mensaje});
}

/// @nodoc
class _$ProblemaCopyWithImpl<$Res> extends _$Estado2CopyWithImpl<$Res>
    implements $ProblemaCopyWith<$Res> {
  _$ProblemaCopyWithImpl(Problema _value, $Res Function(Problema) _then)
      : super(_value, (v) => _then(v as Problema));

  @override
  Problema get _value => super._value as Problema;

  @override
  $Res call({
    Object? mensaje = freezed,
  }) {
    return _then(Problema(
      mensaje == freezed
          ? _value.mensaje
          : mensaje // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$Problema implements Problema {
  const _$Problema(this.mensaje);

  @override
  final String mensaje;

  @override
  String toString() {
    return 'Estado2.problema(mensaje: $mensaje)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is Problema &&
            (identical(other.mensaje, mensaje) ||
                const DeepCollectionEquality().equals(other.mensaje, mensaje)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(mensaje);

  @JsonKey(ignore: true)
  @override
  $ProblemaCopyWith<Problema> get copyWith =>
      _$ProblemaCopyWithImpl<Problema>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function() $default, {
    required TResult Function(int numero, int tipoNumeral) numero,
    required TResult Function(String nombre) imagen,
    required TResult Function(String mensaje) problema,
  }) {
    return problema(mensaje);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
  }) {
    return problema?.call(mensaje);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function()? $default, {
    TResult Function(int numero, int tipoNumeral)? numero,
    TResult Function(String nombre)? imagen,
    TResult Function(String mensaje)? problema,
    required TResult orElse(),
  }) {
    if (problema != null) {
      return problema(mensaje);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(Iniciandome value) $default, {
    required TResult Function(Numero value) numero,
    required TResult Function(Imagen value) imagen,
    required TResult Function(Problema value) problema,
  }) {
    return problema(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
  }) {
    return problema?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(Iniciandome value)? $default, {
    TResult Function(Numero value)? numero,
    TResult Function(Imagen value)? imagen,
    TResult Function(Problema value)? problema,
    required TResult orElse(),
  }) {
    if (problema != null) {
      return problema(this);
    }
    return orElse();
  }
}

abstract class Problema implements Estado2 {
  const factory Problema(String mensaje) = _$Problema;

  String get mensaje => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProblemaCopyWith<Problema> get copyWith =>
      throw _privateConstructorUsedError;
}

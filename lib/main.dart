import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuevo_proyecto_2/bloc_contador/bloc_contador.dart';
import 'package:nuevo_proyecto_2/repositorios/local.dart';

import 'package:nuevo_proyecto_2/repositorios/remoto.dart';
import 'package:nuevo_proyecto_2/vistas/aplicacion.dart';

void main() {
  runApp(Proveedor());
}

class Proveedor extends StatelessWidget {
  const Proveedor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => RepositorioRemoto(),
        ),
        RepositoryProvider(
          create: (context) => RepositorioLocal(),
        ),
      ],
      child: BlocProvider(
          create: (context) => BlocContador(context.read<RepositorioRemoto>()),
          child: Aplicacion()),
    );
  }
}

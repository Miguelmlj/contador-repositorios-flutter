import 'package:flutter/material.dart';

class VistaImagen extends StatelessWidget {
  const VistaImagen(this.nombre, {Key? key}) : super(key: key);
  final String nombre;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image.asset(nombre),
    );
  }
}
import 'package:arabic_numbers/arabic_numbers.dart';
import 'package:flutter/material.dart';
import 'package:nuevo_proyecto_2/app/estilos.dart';
import 'package:numerus/numerus.dart';

class VistaNumero extends StatelessWidget {
  const VistaNumero(this.numero, this.opcionNumeral, {Key? key})
      : super(key: key);
  final int numero;
  final int opcionNumeral;
  @override
  Widget build(BuildContext context) {
    //final estado = context.watch<BlocContador>().state;
    //final int numero = (estado as Numero).numero;

    String texto = numero.toString();
    final arabicNumber = ArabicNumbers();

    if (opcionNumeral == 1) {
      texto = arabicNumber.convert(numero);
    } else if (opcionNumeral == 2) {
      texto = numero.toRomanNumeralString()!;
    }
    // style: estiloNumero,

    return Center(
        child: Text(
      texto,
      style: estiloNumero,
    ));
  }
}
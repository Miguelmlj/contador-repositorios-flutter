import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuevo_proyecto_2/bloc_contador/bloc_contador.dart';
import 'package:nuevo_proyecto_2/bloc_contador/eventos.dart';
import 'package:nuevo_proyecto_2/repositorios/local.dart';
import 'package:nuevo_proyecto_2/repositorios/remoto.dart';

class VistaError extends StatelessWidget {
  const VistaError(this.mensajeError, {Key? key}) : super(key: key);
  final String mensajeError;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(mensajeError),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: OutlinedButton(
                  onPressed: () {
                    context.read<BlocContador>().add(
                        RepositorioCambiado(context.read<RepositorioLocal>()));
                  },
                  child: const Text('Usar Repositorio Local'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: OutlinedButton(
                  onPressed: () {
                    context.read<BlocContador>().add(
                        RepositorioCambiado(context.read<RepositorioRemoto>()));
                  },
                  child: const Text('Reintentar con Remoto'),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

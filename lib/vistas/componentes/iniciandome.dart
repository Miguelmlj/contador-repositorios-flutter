import 'package:flutter/material.dart';
import 'package:nuevo_proyecto_2/app/constantes.dart';
import 'package:nuevo_proyecto_2/app/estilos.dart';

class VistaIniciandome extends StatelessWidget {
  const VistaIniciandome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
                width: 200, height: 200, child: CircularProgressIndicator()),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              mensajeInicializacion,
              style: estiloInicio,
            ),
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuevo_proyecto_2/bloc_contador/bloc_contador.dart';
import 'package:nuevo_proyecto_2/bloc_contador/estados.dart';
import 'package:nuevo_proyecto_2/bloc_contador/eventos.dart';
import 'principal.dart';



class Aplicacion extends StatelessWidget {
  const Aplicacion({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocContador>().state;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text('ejemplo de bloc'),
        actions: [Opciones()],),
        body: VistaPrincipal(),
        floatingActionButton: VistaBotones(estado),
        bottomNavigationBar: Deportes(),
    )
    );
  }
}

class Deportes extends StatefulWidget {
  const Deportes({
    Key? key,
  }) : super(key: key);

  @override
  State<Deportes> createState() => _DeportesState();
}

class _DeportesState extends State<Deportes> {

  final List<String> sports = ['Soccer','Basketball'];
  late int optSelected;

  @override
  void initState() {
    super.initState();
    optSelected = 0;
  }

  @override
  Widget build(BuildContext context) {
    
    final estado = context.watch<BlocContador>().state;
    if(estado is Problema) return BottomAppBar();

    return BottomAppBar(
      child: Row(
        children: [
          ...sports.asMap().entries.map((e) => Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ChoiceChip(
                label: Text(e.value), 
                selected: optSelected==e.key, 
                onSelected: (selected){
                  context.read<BlocContador>().add(ModificadoDeportes(e.key));
                setState(() {
                  optSelected = e.key;
                });
              },
              ),
            ),            
            )).toList(),
        ],),
    );

  }
}

class VistaBotones extends StatelessWidget {
  const VistaBotones(this.estado,{ Key? key }) : super(key: key);
  final estado;
  @override
  Widget build(BuildContext context) {
    //final estado = context.watch<BlocContador>().state;

    if((estado is Numero) || (estado is Imagen)){
    return Row(
            mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FloatingActionButton(onPressed: (){
                context.read<BlocContador>().add(Modificado(1));
              }, child:Icon(Icons.add)),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FloatingActionButton(onPressed: (){
                context.read<BlocContador>().add(Modificado(-1));
              }, child:Icon(Icons.remove)),
            ),
          ],
          );
    }
      return Container();
  }
}

class Opciones extends StatefulWidget {
  Opciones({ Key? key }) : super(key: key);
  
  @override
  State<Opciones> createState() => _OpcionesState();
}

class _OpcionesState extends State<Opciones> {
  
  
  final List<String> titulos = ['Occidente','Arábigo', 'Romanos'];
  late int opcionSeleccionada;

  @override
  void initState(){
    super.initState();
    opcionSeleccionada = 0;
  }

  @override
  Widget build(BuildContext context) {
     final estado = context.watch<BlocContador>().state;

     if(estado is Problema) return Container();    
       return Wrap(
      children: [
        ...titulos.asMap()
        .entries
        .map((e) => Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ChoiceChip(label: Text(e.value), selected: opcionSeleccionada==e.key,
            onSelected: (seleccionado){
              //analizar esto
              context.read<BlocContador>().add(ModificadoNumerales(e.key));
              setState(() {
                opcionSeleccionada = e.key;
              });
            },),
          ),
        )).toList(),
      ],
    );  
  }
}





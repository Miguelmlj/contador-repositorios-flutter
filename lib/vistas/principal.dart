import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuevo_proyecto_2/app/constantes.dart';
import 'package:nuevo_proyecto_2/app/estilos.dart';
import 'package:nuevo_proyecto_2/bloc_contador/bloc_contador.dart';
import 'package:numerus/numerus.dart';
import 'package:arabic_numbers/arabic_numbers.dart';
import 'componentes/componentes.dart';

//import 'package:nuevo_proyecto_2/bloc_contador/estados.dart';

class VistaPrincipal extends StatelessWidget {
  const VistaPrincipal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocContador>().state; //observa el estado

    return estado.map((iniciandome) => VistaIniciandome(),
        numero: (numero) => VistaNumero(numero.numero, numero.tipoNumeral),
        imagen: (imagen) => VistaImagen(imagen.nombre),
        //problema: (problema) => VistaProblema());
        problema: (problema) => VistaError(problema.mensaje));

    // if(estado is Iniciandome){
    //   return VistaIniciandome();
    // }
    // if(estado is Numero){

    //   return VistaNumero(estado.numero);
    // }
    // if(estado is Imagen){
    //   return VistaImagen(estado.nombre);
    // }
  }
}

// class VistaProblema extends StatelessWidget {
//   const VistaProblema({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }









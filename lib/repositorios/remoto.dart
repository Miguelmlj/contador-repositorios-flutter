import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:nuevo_proyecto_2/dominio/interfase_repositorios.dart';
import 'package:fpdart/fpdart.dart';
//import 'package:http_parser/';

class RepositorioRemoto extends InterfaseRepositorios {
  @override
  Future<Either<String,int>> obtenerNumero({required int minimo, required int maximo}) async {
    assert(maximo >= minimo, 'El maximo debe de ser mayor o igual al numero');
    
    final String peticion = 'https://www.random.org/integers/?num=1&min=$minimo&max=$maximo&col=1&base=10&fomat=plain&rnd=new';

    int? entero;
    var respuesta;
    try {
      Uri peticionUrl = Uri.parse(peticion);
      //http.Response respuesta = await http.get(peticionUrl);
      respuesta = await http.get(peticionUrl);
      
      String respuestaCruda = respuesta.body;
      
      entero = int.parse(respuestaCruda);
      
    } on SocketException{
      return Left('No hay conexion a internet');
    }catch (e) {
      
      if(respuesta.statusCode == 503){
        return Left('La petición URI no está escrita correctamente ó el server no está disponible');
      }
      return Left("Problemas en acceso a repositorio remoto");
      
    }
    //print(entero);
    return Right(entero);
  }
}

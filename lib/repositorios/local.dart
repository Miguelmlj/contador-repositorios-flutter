import 'dart:math';

import 'package:nuevo_proyecto_2/dominio/interfase_repositorios.dart';
import 'package:fpdart/fpdart.dart';
class RepositorioLocal extends InterfaseRepositorios {
  @override
  Future<Either<String,int>> obtenerNumero({required int minimo, required int maximo}) {
    assert(maximo >= minimo,'El maximo debe de ser mayor o igual al numero');
    
    int valor = Random().nextInt(maximo - minimo + 1) + minimo;
    return Future.value(Right(valor));
  }
}

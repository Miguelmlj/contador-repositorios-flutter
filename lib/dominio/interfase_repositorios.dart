import 'package:fpdart/fpdart.dart';

abstract class InterfaseRepositorios {
  Future<Either<String, int>> obtenerNumero({
    required int minimo,
    required int maximo,
  });
}